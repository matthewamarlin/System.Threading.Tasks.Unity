﻿//
//  AssemblyInfo.cs
//
//  Author:
//  	Jim Borden  <jim.borden@couchbase.com>
//
//  Original Copyright (c) 2015 Couchbase, Inc All rights reserved.
//  Modified Copyright (c) 2017 TrueSight Studio
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes.
// Change them to the values specific to your project.

[assembly: AssemblyTitle("System.Threading.Tasks.Unity")]
[assembly: AssemblyDescription("The Task Parallel Library ported back to be compatible with Unity3D")]
[assembly: AssemblyCompany ("TrueSight Studio")]
[assembly: AssemblyProduct("System.Threading.Tasks.Unity")]
[assembly: AssemblyCopyright("Copyright © TrueSight Studio 2017")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
