# System.Threading.Tasks.Unity

## Overview
An Implementation of Microsofts [Task Parallel Library (TPL)] backported for [Unity3D]

This code was forked from: https://github.com/couchbasedeps/dotnet-tpl35

## License

This project falls under the falls under the [MIT License] please see the [License] file for more information.

[Unity3D]: https://unity3d.com/
[Task Parallel Library (TPL)]: https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-parallel-library-tpl
[MIT License]: https://en.wikipedia.org/wiki/MIT_License
[License]: https://gitlab.com/matthewamarlin/System.Threading.Tasks.Unity/blob/master/LICENSE